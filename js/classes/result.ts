export enum RESULT {
    HIT = 0,
    MISS = 1,
    DODGE = 2,
    CRIT = 3,
    GLANCE = 4
};

