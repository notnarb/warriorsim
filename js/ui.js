import {spells} from "./data/spells.js";
import {buffs} from "./data/buffs.js";
import {races} from "./data/races";
import {gear, enchant, sets, isTempEnchant} from "./data/gear.js";
import {session} from "./data/session.js";
import {talents, findTalentIndex} from "./data/talents";
import {Player} from "./classes/player.js";
import {Simulation, version} from "./classes/simulation";
import {STATS} from './stats';

const currentGear = {
	"head": 0,
	"neck": 0,
	"shoulder": 0,
	"back": 0,
	"chest": 0,
	"wrist": 0,
	"hands": 0,
	"waist": 0,
	"legs": 0,
	"feet": 0,
	"finger1": 0,
	"finger2": 0,
	"trinket1": 0,
	"trinket2": 0,
	"ranged": 0,
	"mainhand": 0,
	"offhand": 0,
	"twohand": 0,
};

const currentEnchants = {
	"head": 0,
	"shoulder": 0,
	"back": 0,
	"chest": 0,
	"wrist": 0,
	"hands": 0,
	"legs": 0,
	"feet": 0,
	"mainhand": 0,
	"mainhand_temp": 0,
	"offhand": 0,
	"offhand_temp": 0,
	"twohand": 0,
	"twohand_temp": 0,
};

let currentBuffs = [];

let currentSpells = [];

let currentTalents = [
	{
		"n": "Arms",
		"t":[3,0,3,0,5,0,0,1,3,0,2,0,0,0,0,0,0,0],
	}, {
		"n": "Fury",
		"t":[0,5,0,5,0,0,0,5,5,2,5,0,1,0,0,5,1]
	}, {
		"n": "Protection",
		"t":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	}
];

function addEnchant(slot, enchantId) {
	if (isTempEnchant(slot, enchantId)) {
		currentEnchants[slot + "_temp"] = enchantId;
	} else {
		currentEnchants[slot] = enchantId;
	}
}
function removeEnchant(slot, enchantId) {
	const key = isTempEnchant(slot, enchantId) ? `${slot}_temp` : slot;
	if (currentEnchants[key] == enchantId) {
		currentEnchants[key] = 0;
	}
}

export const UI = {

    init: function () {
        var view = this;
        view.variables();
        view.events();
        view.loadSession();
        view.loadWeapons("mainhand");
        view.updateSidebar();

        view.body.on('click', '.wh-tooltip, .tablesorter-default a', function (e) {
            e.preventDefault();
        });

    },

    variables: function () {
        var view = this;
        view.body = $('body');
        view.buffs = view.body.find('article.buffs');
        view.fight = view.body.find('article.fight');
        view.rotation = view.body.find('article.rotation');
        view.talents = view.body.find('article.talents');
        view.filter = view.body.find('article.filter');
        view.main = view.body.find('section.main');
        view.sidebar = view.body.find('section.sidebar');
        view.tcontainer = view.main.find('.table-container');
        view.alerts = view.body.find('.alerts');
        view.progress = view.main.find('progress');
    },

    events: function () {
        var view = this;

        view.sidebar.find('.js-settings').click(function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
            window.scrollTo(0, 0);
            $('section.settings').height(view.body.outerHeight());
            $('section.settings').toggleClass('active');
            view.sidebar.find('.js-stats').removeClass('active');
            $('section.stats').removeClass('active');
        });

        view.sidebar.find('.js-dps').click(function (e) {
            e.preventDefault();
            view.disableEditMode();
            view.startLoading();
            view.simulateDPS();
        });

        view.sidebar.find('.js-stats').click(function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
            $('section.stats').toggleClass('active');
            view.sidebar.find('.js-settings').removeClass('active');
            $('section.settings').removeClass('active');
        });

        view.body.on('click', '.js-table', function(e) {
            e.preventDefault();
            view.disableEditMode();
            let first = view.tcontainer.find('table.gear tbody tr').first();
            view.tcontainer.find('table.gear tbody tr').addClass('waiting');
            view.tcontainer.find('table.gear tbody tr td:last-of-type').html('');
            view.startLoading();
            view.simulateDPS(first);
        });

        view.main.on('click', '.js-enchant', function(e) {
            e.preventDefault();
            view.disableEditMode();
            let first = view.tcontainer.find('table.enchant tbody tr').first();
            view.tcontainer.find('table.enchant tbody tr').addClass('waiting');
            view.tcontainer.find('table.enchant tbody tr td:last-of-type').html('');
            view.startLoading();
            view.simulateDPS(first);
        });

        view.main.on('click', '.js-editmode', function(e) {
            e.preventDefault();
            $(this).toggleClass('active');
            window.scrollTo(0, 0);
            let active = $(this).hasClass('active');
            if (active) view.enableEditMode();
            else view.disableEditMode();
        });

        view.main.find('nav li p').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            let li = $(this).parent();
            li.addClass('active');
            li.siblings().removeClass('active');
            var type = li.data('type');
            if (!type) type = li.parents('[data-type]').data('type');

            if (type == "mainhand" || type == "offhand" || type == "twohand") 
                view.loadWeapons(type);
            else if (type == "custom") 
                view.loadCustom();
            else
                view.loadGear(type);
        });

        view.tcontainer.on('click', 'table.gear td:not(.ppm)', function(e) {
            var table = $(this).parents('table');
            var type = table.data('type');
            var max = table.data('max');
            var tr = $(this).parent();

            if (table.hasClass('editmode')) {
                if (tr.hasClass('hidden'))
                    view.rowShowItem(tr);
                else
                    view.rowHideItem(tr);
                return;
            }

            if (tr.hasClass('active')) {
                view.rowDisableItem(tr);
            }
            else {
                var counter = table.find('tr.active').length;
                if (counter >= max) view.rowDisableItem(table.find('tr.active').last());
                view.rowEnableItem(tr);
            }

            view.updateSession();
            view.updateSidebar();
        });

        view.tcontainer.on('click', 'table.enchant td:not(.ppm)', function(e) {
            var table = $(this).parents('table');
            var tr = $(this).parent();
            var temp = tr.data('temp');

            if (table.hasClass('editmode')) {
                if (tr.hasClass('hidden'))
                    view.rowShowEnchant(tr);
                else
                    view.rowHideEnchant(tr);
                return;
            }

            if (tr.hasClass('active')) {
                view.rowDisableEnchant(tr);
            }
            else {
                let disable = table.find('tr.active[data-temp="' + temp + '"]').first();
                if (disable.length) view.rowDisableEnchant(disable);
                view.rowEnableEnchant(tr);
            }

            view.updateSession();
            view.updateSidebar();
        });
    },

    enableEditMode: function() {
        var view = this;
        let type = view.tcontainer.find('table.gear').attr('data-type');
        if (type == "mainhand" || type == "offhand" || type == "twohand") 
            view.loadWeapons(type, true);
        else if (type == "custom") 
            view.loadCustom(true);
        else
            view.loadGear(type, true);
    },

    disableEditMode: function() {
        var view = this;
        view.main.find('.js-editmode').removeClass('active');
        let type = view.tcontainer.find('table.gear').attr('data-type');
        if (type == "mainhand" || type == "offhand" || type == "twohand") 
            view.loadWeapons(type, false);
        else if (type == "custom") 
            view.loadCustom(false);
        else
            view.loadGear(type, false);
    },
	getBuffs: function() {
		return currentBuffs;
	},
	updateBuffs: function(buffIds) {
		currentBuffs = buffIds.map(buff => parseInt(buff, 10));
	},
	getSpells: function() {
		return currentSpells;
	},
	updateSpells: function(spellIds) {
		currentSpells = spellIds.map(spell => parseInt(spell, 10));
	},
	getTalentCount: function(talent) {
		const [treei, talenti] = findTalentIndex(talent);
		return currentTalents[treei].t[talenti];
	},
	updateTalentCount: function(talent, count) {
		const [treei, talenti] = findTalentIndex(talent);
		currentTalents[treei].t[talenti] = count;
	},
    simulateDPS: function(row) {
        let view = this;
        let dps = view.sidebar.find('#dps');
        let stats = view.sidebar.find('#stats');
        let time = view.sidebar.find('#time');
        let btn = view.sidebar.find('.js-dps');
        dps.text('');
        time.text('');
        var player = new Player(getPlayerConfig());
        if (row) {
            let type = row.parents('table').data('type');
            if (type == "finger" || type == "trinket" || type == "custom")
                player = new Player(getPlayerConfig(), null, type);
        }
        if (!player.mh) {
            view.addAlert('No weapon selected');
            view.endLoading();
            return;
        }

		const worker = new Worker('/dist/sim_worker.js');

		worker.postMessage({
			playerConfig: getPlayerConfig(),
			simConfig: getSimConfig(),
		});
		worker.onmessage = ({data}) => {
			if (data.action === "update") {
				const {totalDmg, totalDuration, iteration, iterations} = data;
				let perc = parseInt(iteration / iterations * 100);
				dps.text((totalDmg / totalDuration).toFixed(2));
				btn.css('background', 'linear-gradient(to right, transparent ' + perc + '%, #444 ' + perc + '%)');
			}
			if (data.action === "finished") {
				const {totalDmg, totalDuration, startTime, endTime, minDps, maxDps} = data;
                dps.text((totalDmg / totalDuration).toFixed(2));
                time.text((endTime - startTime) / 1000);
                stats.html(minDps.toFixed(2) + ' min&nbsp;&nbsp;&nbsp;&nbsp;' + maxDps.toFixed(2) + ' max');
                btn.css('background', '');
                if (row) view.simulateRow(row);
                else view.endLoading();

                // STATS.initCharts(sim);
                // sim = null;
                // player = null;
			}
		};
        // var sim = new Simulation(getSimConfig(), player, 
        //     () => {
        //         // Finished
        //         dps.text((sim.totaldmg / sim.totalduration).toFixed(2));
        //         time.text((sim.endtime - sim.starttime) / 1000);
        //         stats.html(sim.mindps.toFixed(2) + ' min&nbsp;&nbsp;&nbsp;&nbsp;' + sim.maxdps.toFixed(2) + ' max');
        //         btn.css('background', '');
        //         if (row) view.simulateRow(row);
        //         else view.endLoading();

        //         STATS.initCharts(sim);
        //         sim = null;
        //         player = null;
                
        //     },
        //     (iteration) => {
        //         // Update
        //         let perc = parseInt(iteration / sim.iterations * 100);
        //         dps.text((sim.totaldmg / sim.totalduration).toFixed(2));
        //         btn.css('background', 'linear-gradient(to right, transparent ' + perc + '%, #444 ' + perc + '%)');
        //     }
        // );
        // sim.start();
    },

    simulateRow: function(tr) {
        var view = this;
        var dps = tr.find('td:last-of-type');
        var type = tr.parents('table').data('type');
        var item = tr.data('id');
        var isench = tr.parents('table').hasClass('enchant');
        var istemp = tr.data('temp') == true;
        var base = parseFloat(view.sidebar.find('#dps').text());
        var rows = tr.siblings().length + 1;
        var rowsdone = tr.siblings(':not(.waiting)').length;
        var btn = view.sidebar.find('.js-table');

        var player = new Player(getPlayerConfig(), item, type, istemp ? 2 : isench ? 1 : 0);
        var sim = new Simulation(getSimConfig(), player,
            () => {
                // Finished
                let span = $('<span></span>');
                let calc = sim.totaldmg / sim.totalduration;
                let diff = calc - base;
                span.text(diff.toFixed(2));
                if (diff >= 0) span.addClass('p');
                else span.addClass('n');
                dps.text(calc.toFixed(2)).append(span);

                view.tcontainer.find('table').each(function() {
                    if (type == "custom") return;
                    $(this).trigger('update');
                    let sortList = [[$(this).find('th').length - 1, 1]];
                    $(this).trigger("sorton", [sortList]);
                });
                
                tr.removeClass('waiting');
                let perc = parseInt(((rowsdone + 1) * sim.iterations) / (sim.iterations * rows) * 100);
                if (perc == 100) btn.css('background', '');
                else btn.css('background', 'linear-gradient(to right, transparent ' + perc + '%, #444 ' + perc + '%)');
                sim = null;
                player = null;

                if (isench) {
                    for(let i of enchant[type])
                        if (i.id == item)
                            i.dps = calc.toFixed(2);
                }
                else {
                    for(let i of gear[type])
                        if (i.id == item)
                            i.dps = calc.toFixed(2);
                }

                let next = view.tcontainer.find('tbody tr.waiting').first();
                if (next.length) view.simulateRow(next);
                else { view.endLoading(); view.updateSession(); }
            },
            (iteration) => {
                // Update
                let perc = parseInt((rowsdone * sim.iterations + iteration) / (sim.iterations * rows) * 100);
                btn.css('background', 'linear-gradient(to right, transparent ' + perc + '%, #444 ' + perc + '%)');
                dps.text((sim.totaldmg / sim.totalduration).toFixed(2));
            }
        );
        sim.start();
    },

    rowDisableItem: function(tr) {
        var table = tr.parents('table');
        var type = table.data('type');
        tr.removeClass('active');
		if (currentGear[type] == tr.data('id')) {
			currentGear[type] = 0;
		}
    },

    rowEnableItem: function(tr) {
        var table = tr.parents('table');
        var type = table.data('type');
		const id = tr.data('id');
        tr.addClass('active');

		currentGear[type] = id;
		// TODO: there was an:
		//   else if (type != "finger" && type != "trinket" && type != "custom")
		//     item.selected=false
		// here. Assuming that is invalid for finger/trinket but still applicable for "custom"

        if (type == "twohand") {
			currentGear.mainhand = 0;
			currentGear.offhand = 0;
			currentEnchants.mainhand = 0;
			currentEnchants.mainhand_temp = 0;
			currentEnchants.offhand = 0;
			currentEnchants.offhand_temp = 0;
        }

        if (type == "mainhand" || type == "offhand") {
			currentGear.twohand = 0;
			currentEnchants.twohand = 0;
			currentEnchants.twohand_temp = 0;
        }
    },

    rowHideItem: function(tr) {
        var table = tr.parents('table');
        var type = table.data('type');
        tr.removeClass('active');
        tr.addClass('hidden');
        tr.find('.hide').html(eyesvghidden);
        for(let i = 0; i < gear[type].length; i++) {
            if (gear[type][i].id == tr.data('id')) {
                gear[type][i].hidden = true;
				if (currentGear[type] == tr.data('id')){
					currentGear[type] = 0;
				}
            }
        }
    },

    rowShowItem: function(tr) {
        var table = tr.parents('table');
        var type = table.data('type');
        tr.removeClass('hidden');
        tr.find('.hide').html(eyesvg);
        for(let i = 0; i < gear[type].length; i++) {
            if (gear[type][i].id == tr.data('id'))
                gear[type][i].hidden = false;
        }
    },

    rowDisableEnchant: function(tr) {
        var table = tr.parents('table');
        var type = table.data('type');
        tr.removeClass('active');
		const id = tr.data('id');

		removeEnchant(type, id);
    },

    rowEnableEnchant: function(tr) {
        var table = tr.parents('table');
        var type = table.data('type');
        tr.addClass('active');
		const id = tr.data('id');

		addEnchant(type, id);
    },

    rowHideEnchant: function(tr) {
        var table = tr.parents('table');
        var type = table.data('type');
        tr.removeClass('active');
        tr.addClass('hidden');
        tr.find('.hide').html(eyesvghidden);
		const id = tr.data('id');

		removeEnchant(type, id);
        for(let i = 0; i < enchant[type].length; i++) {
            if (enchant[type][i].id == id) {
                enchant[type][i].hidden = true;
            }
        }
    },

    rowShowEnchant: function(tr) {
        var table = tr.parents('table');
        var type = table.data('type');
        tr.removeClass('hidden');
        tr.find('.hide').html(eyesvg);
        for(let i = 0; i < enchant[type].length; i++) {
            if (enchant[type][i].id == tr.data('id'))
                enchant[type][i].hidden = false;
        }
    },

    startLoading: function() {
        let btns = $('.js-dps, .js-table, .js-enchant');
        btns.addClass('loading');
        btns.append('<span class="spinner"><span class="bounce1"></span><span class="bounce2"></span><span class="bounce3"></span></span>');
        $('section.main nav').addClass('loading');
    },

    endLoading: function() {
        let btns = $('.js-dps, .js-table, .js-enchant');
        btns.removeClass('loading');
        btns.find('.spinner').remove();
        $('section.main nav').removeClass('loading');
    },

    updateSidebar: function () {
        var view = this;
        var player = new Player(getPlayerConfig());

        let space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        if (!player.mh) return;
        view.sidebar.find('#str').text(player.stats.str);
        view.sidebar.find('#agi').text(player.stats.agi);
        view.sidebar.find('#ap').text(player.stats.ap);
        view.sidebar.find('#skill').html(player.stats['skill_' + player.mh.type] + ' <small>MH</small>' + (player.oh ? space + player.stats['skill_' + player.oh.type] + ' <small>OH</small>' : ''));
        view.sidebar.find('#hit').html((player.stats.hit || 0) + '%');
        view.sidebar.find('#miss').html(Math.max(player.mh.miss, 0).toFixed(2) + '% <small>1H</small>' + (player.oh ? space + Math.max(player.mh.dwmiss, 0).toFixed(2) + '% <small>DW</small>' : ''));
        let mhcrit = player.crit + player.mh.crit;
        let ohcrit = player.crit + (player.oh ? player.oh.crit : 0);
        view.sidebar.find('#crit').html(mhcrit.toFixed(2) + '% <small>MH</small>' + (player.oh ? space + ohcrit.toFixed(2) + '% <small>OH</small>' : ''));
        let mhcap = 100 - player.mh.dwmiss - player.mh.dodge - player.mh.glanceChance;
        let ohcap = player.oh ? 100 - player.oh.dwmiss - player.oh.dodge - player.oh.glanceChance : 0;
        view.sidebar.find('#critcap').html(mhcap.toFixed(2) + '% <small>MH</small>'+ (player.oh ? space + ohcap.toFixed(2) + '% <small>OH</small>' : ''));
        let mhdmg = player.stats.dmgmod * player.mh.modifier * 100;
        let ohdmg = player.stats.dmgmod * (player.oh ? player.oh.modifier * 100 : 0);
        view.sidebar.find('#dmgmod').html(mhdmg.toFixed(2) + '% <small>MH</small>' + (player.oh ? space + ohdmg.toFixed(2) + '% <small>OH</small>' : ''));
        view.sidebar.find('#haste').html((player.stats.haste * 100).toFixed(2) + '%');
        view.sidebar.find('#race').text(localStorage.race);
        view.sidebar.find('#sets').empty();

        for (let set of sets) {
            let counter = 0;
            for (let item of set.items)
                if (player.items.includes(item))
                    counter++;
            if (counter == 0)
                continue;
            if (counter >= set.bonus[0].count)
                view.sidebar.find('#sets').append(`<a href="https://classic.wowhead.com/item-set=${set.id}" class="q4">${set.name} (${counter})</a><br />`);
        }
    },

    updateSession: function () {
        var view = this;

        localStorage.level = view.fight.find('input[name="level"]').val();
        localStorage.race = view.fight.find('select[name="race"]').val();
        localStorage.simulations = view.fight.find('input[name="simulations"]').val();
        localStorage.timesecsmin = view.fight.find('input[name="timesecsmin"]').val();
        localStorage.timesecsmax = view.fight.find('input[name="timesecsmax"]').val();
        localStorage.executeperc = view.fight.find('input[name="executeperc"]').val();
        localStorage.startrage = view.fight.find('input[name="startrage"]').val();
        localStorage.targetlevel = view.fight.find('input[name="targetlevel"]').val();
        localStorage.targetarmor = view.fight.find('input[name="targetarmor"]').val();
        localStorage.targetresistance = view.fight.find('input[name="targetresistance"]').val();
        localStorage.adjacent = view.fight.find('input[name="adjacent"]').val();
        localStorage.adjacentlevel = view.fight.find('input[name="adjacentlevel"]').val();
        localStorage.aqbooks = view.fight.find('select[name="aqbooks"]').val();
        localStorage.weaponrng = view.fight.find('select[name="weaponrng"]').val();
        localStorage.spelldamage = view.fight.find('input[name="spelldamage"]').val();

        localStorage.buffs = JSON.stringify(currentBuffs);
		localStorage.currentGear = JSON.stringify(currentGear);
		localStorage.currentEnchants = JSON.stringify(currentEnchants);
		localStorage.currentSpells = JSON.stringify(currentSpells);
		localStorage.talents = JSON.stringify(currentTalents);

        let _sources = [], _phases = [], _gear = {};
        view.filter.find('.sources .active').each(function () { _sources.push($(this).attr('data-id')); });
        view.filter.find('.phases .active').each(function () { _phases.push($(this).attr('data-id')); });

        for (let type in gear) {
			_gear[type] = [];
            for (let item of gear[type]) {
                _gear[type].push({id:item.id,dps:item.dps,hidden:item.hidden});
            }
        }

        localStorage.sources = JSON.stringify(_sources);
        localStorage.phases = JSON.stringify(_phases);
		localStorage.gear = JSON.stringify(_gear);
    },

    loadSession: function () {
        var view = this;

        if (!localStorage.length) view.firstSession();

        for (let prop in localStorage) {
            view.fight.find('input[name="' + prop + '"]').val(localStorage[prop]);
            view.fight.find('select[name="' + prop + '"]').val(localStorage[prop]);
        }

        view.sidebar.find('.bg').attr('data-race', view.fight.find('select[name="race"]').val());

        let _buffs = !localStorage.buffs ? JSON.parse(session.buffs) : JSON.parse(localStorage.buffs);
        let _rotation = !localStorage.rotation ? JSON.parse(session.rotation) : JSON.parse(localStorage.rotation);
        let _sources = !localStorage.sources ? JSON.parse(session.sources) : JSON.parse(localStorage.sources);
        let _phases = !localStorage.phases ? JSON.parse(session.phases) : JSON.parse(localStorage.phases);
        let _talents = !localStorage.talents ? JSON.parse(session.talents) : JSON.parse(localStorage.talents);
        let _gear = !localStorage.gear ? JSON.parse(session.gear) : JSON.parse(localStorage.gear);
        let _enchant = !localStorage.enchant ? JSON.parse(session.enchant) : JSON.parse(localStorage.enchant);
		const _currentGear = localStorage.currentGear ? JSON.parse(localStorage.currentGear) : null;
		const _currentEnchants = localStorage.currentEnchants ? JSON.parse(localStorage.currentEnchants) : null;
		const _currentSpells = localStorage.currentSpells ? JSON.parse(localStorage.currentSpells) : null;

		currentTalents = _talents;

		this.updateBuffs(_buffs);

		if (_currentSpells) {
			this.updateSpells(_currentSpells);
		} else {
			this.updateSpells(_rotation.filter((spell) => spell.active).map(spell => spell.id));
		}

        for (let i of _sources)
            view.filter.find(`.sources [data-id="${i}"]`).addClass('active');

        for (let i of _phases)
            view.filter.find(`.phases [data-id="${i}"]`).addClass('active');

		if (_currentGear) {
			Object.assign(currentGear, _currentGear);
		} else {
			// compatibility with legacy session.
			for (let [type, items] of Object.entries(_gear)) {
				if (currentGear[type] === undefined)
					continue;
				const item = items.find(item => item.selected);
				currentGear[type] = item ? item.id : 0;
			}
		}

		if (_currentEnchants) {
			Object.assign(currentEnchants, _currentEnchants);
		} else {
			// compatibility with legacy session.
			for (let [type, items] of Object.entries(_enchant)) {
				if (currentEnchants[type] === undefined)
					continue;
				// note: weapons may have two selected enchants.
				const enchants = items.filter(e => e.selected);
				for (const e of enchants) {
					addEnchant(type, e ? e.id : 0);
				}
			}
		}

        for (let type in _gear)
            for (let i of _gear[type])
                if (gear[type])
                    for (let j of gear[type])
                        if (i.id == j.id) {
                            j.dps = i.dps;
                            j.hidden = i.hidden;
                        }

        for (let type in _enchant)
            for (let i of _enchant[type])
                for (let j of enchant[type])
                    if (i.id == j.id) {
                        j.dps = i.dps;
                        j.hidden = i.hidden;
                    }

        if (!localStorage.version || parseInt(localStorage.version) < version) view.newVersion();
    },

    filterGear: function () {
        var view = this;
        var type = view.main.find('nav > ul > li.active').data('type');
        if (type == "mainhand" || type == "offhand") 
            view.loadWeapons(type);
        else if (type == "custom") 
            view.loadCustom();
        else 
            view.loadGear(type);
    },

    loadWeapons: function (type, editmode) {
        var view = this;
        var filter = view.main.find('nav li.active .filter .active').text();

        let table = `<table class="gear ${editmode ? 'editmode' : ''}" data-type="${type}" data-max="1">
                        <thead>
                            <tr>
                                ${editmode ? '<th></th>' : ''}
                                <th>Name</th>
                                <th>Source</th>
                                <th>Sta</th>
                                <th>Str</th>
                                <th>Agi</th>
                                <th>AP</th>
                                <th>Crit</th>
                                <th>Hit</th>
                                <th>Min</th>
                                <th>Max</th>
                                <th>Speed</th>
                                <th>Skill</th>
                                <th>Type</th>
                                <th>PPM</th>
                                <th>DPS</th>
                            </tr>
                        </thead>
                    <tbody>`;

        for (let item of gear[type]) {

            if (filter && filter != "All") {
                if (filter == "Mace & Sword") {
                    if (item.type != "Mace" && item.type != "Sword") continue;
                }
                else if (filter == "Axe, Dagger & Sword") {
                    if (item.type != "Axe"  && item.type != "Dagger" && item.type != "Sword") continue; 
                }
                else if (item.type != filter)
                    continue;
            }

            let source = item.source.toLowerCase(), phase = item.phase;
            if (item.source == 'Lethon' || item.source == 'Emeriss' || item.source == 'Kazzak' || item.source == 'Azuregos' || item.source == 'Ysondre' || item.source == 'Taerar' || item.source == 'Green Dragons')
                source = 'worldboss';

            if (phase && !view.filter.find('.phases [data-id="' + phase + '"]').hasClass('active'))
                continue;
            if (source && !view.filter.find('.sources [data-id="' + source + '"]').hasClass('active'))
                continue;

            if (item.hidden && !editmode) continue;

            let tooltip = item.id, rand = '';
            if (tooltip == 199211) tooltip = 19921;
            if (item.rand) rand = '?rand=' + item.rand;
                
            const isSelected = currentGear[type] === item.id;
            table += `<tr data-id="${item.id}" data-name="${item.name}" class="${isSelected ? 'active' : ''} ${item.hidden ? 'hidden' : ''}">
                        ${editmode ? '<td class="hide">' + (item.hidden ? eyesvghidden : eyesvg) + '</td>' : ''}
                        <td><a href="https://classic.wowhead.com/item=${tooltip}${rand}"></a>${item.name}</td>
                        <td>${item.source}</td>
                        <td>${item.sta || ''}</td>
                        <td>${item.str || ''}</td>
                        <td>${item.agi || ''}</td>
                        <td>${item.ap || ''}</td>
                        <td>${item.crit || ''}</td>
                        <td>${item.hit || ''}</td>
                        <td>${item.minhit || ''}</td>
                        <td>${item.maxhit || ''}</td>
                        <td>${item.speed || ''}</td>
                        <td>${item.skill || ''}</td>
                        <td>${item.type || ''}</td>
                        <td class="ppm"><p contenteditable="true">${item.ppm || ''}</p></td>
                        <td>${item.dps || ''}</td>
                    </tr>`;
        }

        table += '</tbody></table></section>';

        view.tcontainer.empty();
        view.tcontainer.append(table);
        view.tcontainer.find('table.gear').tablesorter({
            widthFixed: true,
            sortList: editmode ?  [[15, 1],[1, 0]] : [[14, 1],[0, 0]],
            textSorter : {
                14 : function(a, b, direction, column, table) {
                    var a = parseFloat(a.substring(0,a.indexOf('.') + 3));
                    var b = parseFloat(b.substring(0,b.indexOf('.') + 3));
                    if (isNaN(a)) a = 0; 
                    if (isNaN(b)) b = 0; 
                    return (a < b) ? -1 : (a > b) ? 1 : 0;
                },
            },
            headers: {
                14: { sorter: "text" }
            }
        });

        view.loadEnchants(type, editmode);
    },

    loadGear: function (type, editmode) {
        var view = this;

        var max = 1;
        let table = `<table class="gear ${editmode ? 'editmode' : ''}" data-type="${type}" data-max="${max}">
                        <thead>
                            <tr>
                                ${editmode ? '<th></th>' : ''}
                                <th>Name</th>
                                <th>Source</th>
                                <th>Sta</th>
                                <th>Str</th>
                                <th>Agi</th>
                                <th>AP</th>
                                <th>Hit</th>
                                <th>Crit</th>
                                <th>Skill</th>
                                <th>Type</th>
                                <th>DPS</th>
                            </tr>
                        </thead>
                    <tbody>`;

        for (let item of gear[type]) {

            let source = item.source.toLowerCase(), phase = item.phase;
            if (item.source == 'Lethon' || item.source == 'Emeriss' || item.source == 'Kazzak' || item.source == 'Azuregos' || item.source == 'Ysondre' || item.source == 'Taerar' || item.source == 'Green Dragons')
                source = 'worldboss';

			// TODO: what?
            if (max == 2 && 
                ((phase && !view.filter.find('.phases [data-id="' + phase + '"]').hasClass('active')) ||
                (source && !view.filter.find('.sources [data-id="' + source + '"]').hasClass('active'))))
                item.selected = false;

            if (phase && !view.filter.find('.phases [data-id="' + phase + '"]').hasClass('active'))
                continue;
            if (source && !view.filter.find('.sources [data-id="' + source + '"]').hasClass('active'))
                continue;

            if (item.hidden && !editmode) continue;

            let tooltip = item.id, rand = '';
            if (tooltip == 145541) tooltip = 14554;
            if (tooltip == 198981) tooltip = 19898;
            if (item.rand) rand = '?rand=' + item.rand;

			const isSelected = item.id === currentGear[type];
            table += `<tr data-id="${item.id}" class="${isSelected ? 'active' : ''} ${item.hidden ? 'hidden' : ''}">
                        ${editmode ? '<td class="hide">' + (item.hidden ? eyesvghidden : eyesvg) + '</td>' : ''}
                        <td><a href="https://classic.wowhead.com/item=${tooltip}${rand}"></a>${item.name}</td>
                        <td>${item.source || ''}</td>
                        <td>${item.sta || ''}</td>
                        <td>${item.str || ''}</td>
                        <td>${item.agi || ''}</td>
                        <td>${item.ap || ''}</td>
                        <td>${item.hit || ''}</td>
                        <td>${item.crit || ''}</td>
                        <td>${item.skill || ''}</td>
                        <td>${item.type || ''}</td>
                        <td>${item.dps || ''}</td>
                    </tr>`;
        }

        table += '</tbody></table></section>';

        view.tcontainer.empty();
        view.tcontainer.append(table);
        view.tcontainer.find('table.gear').tablesorter({
            widthFixed: true,
            sortList: editmode ? [[11, 1],[1, 0]] : [[10, 1],[0, 0]],
            textSorter : {
                10 : function(a, b, direction, column, table) {
                    var a = parseFloat(a.substring(0,a.indexOf('.') + 3));
                    var b = parseFloat(b.substring(0,b.indexOf('.') + 3));
                    if (isNaN(a)) a = 0; 
                    if (isNaN(b)) b = 0; 
                    return (a < b) ? -1 : (a > b) ? 1 : 0;
                },
            },
            headers: {
                10: { sorter: "text" }
            }
        });

        view.loadEnchants(type, editmode);
        view.updateSession();
        view.updateSidebar();
    },

    loadCustom: function (editmode) {
        var view = this;

        let table = `<table class="gear ${editmode ? 'editmode' : ''}" data-type="custom" data-max="10">
                        <thead>
                            <tr>
                                ${editmode ? '<th></th>' : ''}
                                <th>Name</th>
                                <th>Str</th>
                                <th>Agi</th>
                                <th>AP</th>
                                <th>Hit</th>
                                <th>Crit</th>
                                <th>Skill</th>
                                <th>DPS</th>
                            </tr>
                        </thead>
                    <tbody>`;

        for (let item of gear.custom) {
            if (item.hidden && !editmode) continue;
            table += `<tr data-id="${item.id}" class="${item.selected ? 'active' : ''} ${item.hidden ? 'hidden' : ''}">
                        ${editmode ? '<td class="hide">' + (item.hidden ? eyesvghidden : eyesvg) + '</td>' : ''}
                        <td>${item.name}</td>
                        <td>${item.str || ''}</td>
                        <td>${item.agi || ''}</td>
                        <td>${item.ap || ''}</td>
                        <td>${item.hit || ''}</td>
                        <td>${item.crit || ''}</td>
                        <td>${item.skill_1 || ''}</td>
                        <td>${item.dps || ''}</td>
                    </tr>`;
        }

        table += '</tbody></table></section>';

        view.tcontainer.empty();
        view.tcontainer.append(table);
        view.tcontainer.find('table.gear').tablesorter({
            widthFixed: true,
            sortList: editmode ? [[11, 1]] : [[10, 1]],
        });
    },

    loadEnchants: function (type, editmode) {
        var view = this;
        view.main.find('.js-enchant').hide();

        if (!enchant[type] || enchant[type].length == 0) return;

        let table = `<table class="enchant ${editmode ? 'editmode' : ''}" data-type="${type}" data-max="1">
                        <thead>
                            <tr>
                                ${editmode ? '<th></th>' : ''}
                                <th>Enchant</th>
                                <th>Str</th>
                                <th>Agi</th>
                                <th>AP</th>
                                <th>Haste</th>
                                <th>Crit</th>
                                <th>Damage</th>
                                <th>PPM</th>
                                <th>DPS</th>
                            </tr>
                        </thead>
                    <tbody>`;

        for (let item of enchant[type]) {

            if (item.phase && !view.filter.find('.phases [data-id="' + item.phase + '"]').hasClass('active'))
                continue;

            if (item.hidden && !editmode) continue;

			const enchantKey = !!item.temp ? `${type}_temp` : type;
			const isSelected = currentEnchants[enchantKey] == item.id;
            table += `<tr data-id="${item.id}" data-temp="${item.temp || false}" class="${isSelected ? 'active' : ''} ${item.hidden ? 'hidden' : ''}">
                        ${editmode ? '<td class="hide">' + (item.hidden ? eyesvghidden : eyesvg) + '</td>' : ''}
                        <td><a href="https://classic.wowhead.com/${item.spellid ? 'spell' : 'item'}=${item.id}"></a>${item.name}</td>
                        <td>${item.str || ''}</td>
                        <td>${item.agi || ''}</td>
                        <td>${item.ap || ''}</td>
                        <td>${item.haste || ''}</td>
                        <td>${item.crit || ''}</td>
                        <td>${item.dmg || ''}</td>
                        <td>${item.ppm || ''}</td>
                        <td>${item.dps || ''}</td>
                    </tr>`;
        }

        table += '</tbody></table></section>';

        if ($(table).find('tbody tr').length == 0) return;

        view.tcontainer.append(table);
        view.tcontainer.find('table.enchant').tablesorter({
            widthFixed: true,
            sortList: editmode ? [[9, 1]] : [[8, 1]],
        });

        view.main.find('.js-enchant').show();
    },

    addAlert: function (msg) {
        var view = this;
        view.alerts.empty().append('<div class="alert"><p>' + msg + '</p></div>');
        view.alerts.find('.alert').click(function () { view.closeAlert(); });
        setTimeout(function () { view.alerts.find('.alert').addClass('in-up') });
        setTimeout(function () { view.closeAlert(); }, 4000);
    },
    
    closeAlert: function () {
        var view = this;
        view.alerts.find('.alert').removeClass('in-up');
        setTimeout(function () { view.alerts.empty(); }, 1000);
    },

    firstSession: function () {
        console.log('Welcome!');
    },

    newVersion: function() {
        var view = this;

        localStorage.version = version;

        if (!view.filter.find(`.phases [data-id="4"]`).hasClass('active'))
            setTimeout(() => { view.filter.find(`.phases [data-id="4"]`).click() }, 100);

    }
    


};

function getPlayerConfig() {
	return {
		aqBooks: $('select[name="aqbooks"]').val() == "Yes",
		weaponRng: $('select[name="weaponrng"]').val() == "Yes",
		spellDamage: parseInt($('input[name="spelldamage"]').val()),
		race: getRace(),
		talents: getTalents(),
		target: getTargetConfig(),
		currentGear: currentGear,
		currentEnchants: currentEnchants,
		currentBuffs: currentBuffs,
		currentSpells: currentSpells,
	};
}

function getTargetConfig() {
	return {
	    level: parseInt($('input[name="targetlevel"]').val()),
        basearmor: parseInt($('input[name="targetarmor"]').val()),
        armor: parseInt($('input[name="targetarmor"]').val()),
        defense: parseInt($('input[name="targetlevel"]').val()) * 5,
        mitigation: 1 - 15 * (parseInt($('input[name="targetresistance"]').val()) / 6000),
        binaryresist: parseInt(10000 - (8300 * (1 - (parseInt($('input[name="targetresistance"]').val()) * 0.15 / 60))))
	};
}

function getRace() {
	const raceName = $('select[name="race"]').val();
	for (let race of races) {
        if (race.name == raceName) {
			return race;
        }
    }

	throw new Error(`Could not find race ${raceName}`);
}

function getTalents() {
	const playerTalents = {};
	for (let treei = 0; treei < 3; treei++) {
        for (let talenti = 0; talenti < talents[treei].t.length; talenti++) {
            $.extend(playerTalents, talents[treei].t[talenti].aura(currentTalents[treei].t[talenti]));
        }
    }
	return playerTalents;
}

function getSimConfig() {
	return {
		timesecsmin: parseInt($('input[name="timesecsmin"]').val()),
		timesecsmax: parseInt($('input[name="timesecsmax"]').val()),
		executeperc: parseInt($('input[name="executeperc"]').val()),
		startrage: parseInt($('input[name="startrage"]').val()),
		iterations: parseInt($('input[name="simulations"]').val()),
	};
}

var eyesvg = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15 12c0 1.654-1.346 3-3 3s-3-1.346-3-3 1.346-3 3-3 3 1.346 3 3zm9-.449s-4.252 8.449-11.985 8.449c-7.18 0-12.015-8.449-12.015-8.449s4.446-7.551 12.015-7.551c7.694 0 11.985 7.551 11.985 7.551zm-7 .449c0-2.757-2.243-5-5-5s-5 2.243-5 5 2.243 5 5 5 5-2.243 5-5z"/></svg>';
var eyesvghidden = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M11.885 14.988l3.104-3.098.011.11c0 1.654-1.346 3-3 3l-.115-.012zm8.048-8.032l-3.274 3.268c.212.554.341 1.149.341 1.776 0 2.757-2.243 5-5 5-.631 0-1.229-.13-1.785-.344l-2.377 2.372c1.276.588 2.671.972 4.177.972 7.733 0 11.985-8.449 11.985-8.449s-1.415-2.478-4.067-4.595zm1.431-3.536l-18.619 18.58-1.382-1.422 3.455-3.447c-3.022-2.45-4.818-5.58-4.818-5.58s4.446-7.551 12.015-7.551c1.825 0 3.456.426 4.886 1.075l3.081-3.075 1.382 1.42zm-13.751 10.922l1.519-1.515c-.077-.264-.132-.538-.132-.827 0-1.654 1.346-3 3-3 .291 0 .567.055.833.134l1.518-1.515c-.704-.382-1.496-.619-2.351-.619-2.757 0-5 2.243-5 5 0 .852.235 1.641.613 2.342z"/></svg>';
