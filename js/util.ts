export function rng(min: number, max: number): number {
    return ~~(Math.random() * (max - min + 1) + min);
}

export function rng10k(): number {
    return ~~(Math.random() * 10000);
}

export function avg(min: number, max: number): number {
    return (min + max) / 2;
}
