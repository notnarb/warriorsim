import {spells} from "./data/spells";
import {buffs} from "./data/buffs";
import {talents} from "./data/talents";
import {UI} from './ui';

export const SETTINGS = {

    init: function () {
        var view = this;
        view.variables();
        view.events();
        view.buildSpells();
        view.buildBuffs();
        view.buildTalents();
    },

    variables: function () {
        var view = this;
        view.body = $('body');
        view.buffs = view.body.find('article.buffs');
        view.fight = view.body.find('article.fight');
        view.rotation = view.body.find('article.rotation');
        view.talents = view.body.find('article.talents');
        view.filter = view.body.find('article.filter');
        view.close = view.body.find('section.settings .btn-close');
        view.bg = view.body.find('section.sidebar .bg');
    },

    events: function () {
        var view = this;

        view.close.click(function (e) {
            e.preventDefault();
            $('.js-settings').removeClass('active');
            $('section.settings').removeClass('active');
        });

        view.buffs.on('click', '.icon', function () {
            let obj = $(this).toggleClass('active');
            if (obj.hasClass('active')) {
                if (obj.data('group'))
                    obj.siblings().filter('[data-group="' + obj.data('group') + '"]').removeClass('active');
                if (obj.data('disable-spell'))
                    $('.rotation [data-id="' + obj.data('disable-spell') + '"]').removeClass('active');
            }

			const buffIds = buffs
				  .map(buff => buff.id)
				  .filter(id => view.buffs.find('[data-id="' + id + '"]').hasClass('active'));

			UI.updateBuffs(buffIds);
            UI.updateSession();
            UI.updateSidebar();
        });

        view.talents.on('click', '.icon', function (e) {
            let talent = view.getTalent($(this));
			const prev = UI.getTalentCount(talent);
            const newValue = Math.min(prev + 1, talent.m);
            $(this).attr('data-count', newValue);
            if (newValue >= talent.m) $(this).addClass('maxed');
            if (talent.enable)
                $('.rotation [data-id="' + talent.enable + '"]').removeClass('hidden');
            $(this).find('a').attr('href', 'https://classic.wowhead.com/spell=' + talent.s[newValue ? 0 : newValue - 1]);
			UI.updateTalentCount(talent, newValue);
            UI.updateSession();
            UI.updateSidebar();
        });

        view.talents.on('contextmenu', '.icon', function (e) {
            e.preventDefault();
            let talent = view.getTalent($(this));
			const prev = UI.getTalentCount(talent);
			const newValue = Math.max(prev - 1, 0);
            $(this).attr('data-count', newValue);
            $(this).removeClass('maxed');
            if (newValue == 0 && talent.enable) {
                $('.rotation [data-id="' + talent.enable + '"]').removeClass('active').addClass('hidden');
				const talentSpellId = parseInt(talent.enable, 10);
				const ids = UI.getSpells().filter(spellId => spellId != talentSpellId);
				UI.updateSpells(ids);
            }
            $(this).find('a').attr('href', 'https://classic.wowhead.com/spell=' + talent.s[newValue == 0 ? 0 : newValue - 1]);
			UI.updateTalentCount(talent, newValue);
            UI.updateSession();
            UI.updateSidebar();
        });

        view.filter.on('click', '.sources li', function (e) {
            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                let id = $(this).data('id');
                view.filter.find(`.phases [data-sources*="${id}"]`).addClass('active');
            }
            UI.updateSession();
            UI.filterGear();
        });

        view.filter.on('click', '.phases li', function (e) {
            $(this).toggleClass('active');
            let sources = $(this).data('sources').split(',');
            let show = $(this).hasClass('active');
            for (let source of sources) {
                if (show) view.filter.find('.sources [data-id="' + source + '"]').addClass('active');
                else view.filter.find('.sources [data-id="' + source + '"]').removeClass('active');
            }
            UI.updateSession();
            UI.filterGear();
        });

        view.rotation.on('click', '.spell', function (e) {
            let t = e.target;
            if (t.nodeName == "LI" || t.nodeName == "INPUT")
                return;
            $(this).toggleClass('active');
			const isActive = $(this).hasClass('active');
            const id = parseInt($(this).data('id'), 10);
			const currentSpells = UI.getSpells();

			const newSpells =
				  isActive ? [...currentSpells, id] : currentSpells.filter(sid => sid !== id);

			UI.updateSpells(newSpells);
            UI.updateSession();
        });

        view.rotation.on('keyup', 'input[type="text"]', function (e) {
            let id = $(this).parents('.spell').data('id');
            for (let spell of spells) {
                if (spell.id == id)
                    spell[$(this).attr('name')] = $(this).val();
            }
            UI.updateSession();
        });

        view.fight.on('change', 'select[name="race"]', function (e) {
            var val = $(this).val();
            var bloodfury = view.rotation.find('[data-id="20572"]');
            var berserking = view.rotation.find('[data-id="26296"]');
            var disableSpell;

            if (val == "Orc") {
                bloodfury.removeClass('hidden');
            }
            else {
                bloodfury.addClass('hidden').removeClass('active');
                disableSpell = 20572;
            }
            if (val == "Troll") {
                berserking.removeClass('hidden');
            }
            else {
                berserking.addClass('hidden').removeClass('active');
                disableSpell = 26296;
            }

			const spellIds = UI.getSpells();
			UI.updateSpells(spellIds.filter((id) => id != disableSpell));

            view.bg.attr('data-race', val);

            UI.updateSession();
            UI.updateSidebar();
        });

        view.fight.on('change', 'select[name="aqbooks"]', function (e) {
            UI.updateSession();
            UI.updateSidebar();
        });

        view.fight.on('keyup', 'input[type="text"]', function (e) {
            UI.updateSession();
            UI.updateSidebar();
        });

        view.fight.on('change', 'select[name="weaponrng"]', function (e) {
            UI.updateSession();
            UI.updateSidebar();
        });

    },

    buildSpells: function () {
        var view = this;
		const currentSpells = new Set(UI.getSpells());
        for (let spell of spells) {

            let tooltip = spell.id == 115671 ? 11567 : spell.id;
            let div = $(`<div data-id="${spell.id}" class="spell"><div class="icon">
            <img src="dist/img/${spell.iconname.toLowerCase()}.jpg " alt="${spell.name}">
            <a href="https://classic.wowhead.com/spell=${tooltip}" class="wh-tooltip"></a>
            </div><ul class="options"></ul></div>`);

            if (spell.timetoend !== undefined)
                div.find('.options').append(`<li>Use on last <input type="text" name="timetoend" value="${spell.timetoend}" data-numberonly="true" /> seconds</li>`);
            if (spell.minrage !== undefined)
                div.find('.options').append(`<li>Use when above <input type="text" name="minrage" value="${spell.minrage}" data-numberonly="true" /> rage</li>`);
            if (spell.maxrage !== undefined)
                div.find('.options').append(`<li>Use when below <input type="text" name="maxrage" value="${spell.maxrage}" data-numberonly="true" /> rage</li>`);
            if (spell.globals !== undefined)
                div.find('.options').append(`<li>Use on first <input type="text" name="globals" value="${spell.globals}" data-numberonly="true" /> globals</li>`);
            if (spell.maincd !== undefined)
                div.find('.options').append(`<li>BT/MS cooldown >= <input type="text" name="maincd" value="${spell.maincd}" data-numberonly="true" /> secs</li>`);
            if (spell.crusaders !== undefined)
                div.find('.options').append(`<li>when <input type="text" name="crusaders" value="${spell.crusaders}" data-numberonly="true" /> crusaders are up</li>`);
            if (spell.haste !== undefined)
                div.find('.options').append(`<li>Attack speed at <input type="text" name="haste" value="${spell.haste}" data-numberonly="true" /> %</li>`);
            if (spell.priorityap !== undefined)
                div.find('.options').append(`<li>Prioritize BT/MS when >= <input style="width:25px" type="text" name="priorityap" value="${spell.priorityap}" data-numberonly="true" /> AP</li>`);
            if (spell.id == 23255)
                div.find('.options').append(`<li>Include Deep Wounds damage</li>`);
            if (spell.id == 11605)
                div.find('.options').append(`<li>Slam macro with MH swing</li>`);
            if (spell.id == 2687)
                div.find('.options').append('<li>Used on cooldown below 80 rage</li>');
            if (spell.reaction !== undefined)
                div.find('.options').append(`<li><input style="width:25px" type="text" name="reaction" value="${spell.reaction}" data-numberonly="true" /> ms reaction time</li>`);
            if (spell.hidden)
                div.addClass('hidden');
            if (localStorage.race == "Orc" && spell.id == 20572)
                div.removeClass('hidden');
            if (localStorage.race == "Troll" && spell.id == 26296)
                div.removeClass('hidden');
			if (currentSpells.has(spell.id))
                div.addClass('active');

            if (spell.maincd !== undefined) {
                div.find('.options li:first-of-type').append(' or');
            }

            if (spell.crusaders !== undefined) {
                div.find('.options li:first-of-type').append(' or');
            }

            if (spell.id == 11567) {
                div.find('.options').empty();
                div.find('.options').append(`<li>Queue when above <input type="text" name="minrage" value="30" data-numberonly="true"> rage or BT/MS cooldown >= <input type="text" name="maincd" value="4" data-numberonly="true"> secs</li>`);
                div.find('.options').append(`<li>Unqueue if below <input type="text" name="unqueue" value="${spell.unqueue}" data-numberonly="true" /> rage, <input type="text" name="unqueuetimer" value="${spell.unqueuetimer}" data-numberonly="true" /> ms before MH swing</li>`);
                div.find('.options').append(`<li><input style="width:25px" type="text" name="reaction" value="${spell.reaction}" data-numberonly="true" /> ms reaction time</li>`);
            }

            if (spell.id == 115671) {
                div.find('.options').empty();
                div.find('.options').before('<label>Execute phase HS:</label>');
                div.find('.options').append(`<li>Queue when above <input type="text" name="minrage" value="30" data-numberonly="true"> rage</li>`);
                div.find('.options').append(`<li>Unqueue if below <input type="text" name="unqueue" value="${spell.unqueue}" data-numberonly="true" /> rage, <input type="text" name="unqueuetimer" value="${spell.unqueuetimer}" data-numberonly="true" /> ms before MH swing</li>`);
                div.find('.options').append(`<li><input style="width:25px" type="text" name="reaction" value="${spell.reaction}" data-numberonly="true" /> ms reaction time</li>`);
            }

            if (spell.id == 11585) {
                div.find('.options').empty();
                div.find('.options').append(`<li>Use when below <input type="text" name="maxrage" value="${spell.maxrage}" data-numberonly="true" /> rage and</li>`);
                div.find('.options').append(`<li>BT/MS cooldown >= <input type="text" name="maincd" value="${spell.maincd}" data-numberonly="true" /> secs</li>`);
                div.find('.options').append(`<li><input style="width:25px" type="text" name="reaction" value="${spell.reaction}" data-numberonly="true" /> ms reaction time</li>`);
            }

            view.rotation.append(div);
        }

        view.rotation.children().eq(3).appendTo(view.rotation);
        view.rotation.children().eq(19).appendTo(view.rotation);
    },

    buildBuffs: function () {
		const currentBuffs = new Set(UI.getBuffs());

        var view = this;
        for (let buff of buffs) {
            let wh = buff.spellid ? 'spell' : 'item';
            let active = currentBuffs.has(buff.id) ? 'active' : '';
            let group = buff.group ? `data-group="${buff.group}"` : '';
            let disable = buff.disableSpell ? `data-disable-spell="${buff.disableSpell}"` : '';
            let html = `<div data-id="${buff.id}" class="icon ${active}" ${group} ${disable}>
                            <img src="dist/img/${buff.iconname.toLowerCase()}.jpg " alt="${buff.name}">
                            <a href="https://classic.wowhead.com/${wh}=${buff.id}" class="wh-tooltip"></a>
                        </div>`;
            view.buffs.append(html);
        }
    },

    buildTalents: function () {
        var view = this;
        for (let tree of talents) {
            let table = $('<table><tr><th colspan="4">' + tree.n + '</th></tr></table>');
            for (let i = 0; i < 7; i++) table.prepend('<tr><td></td><td></td><td></td><td></td></tr>');
            for (let talent of tree.t) {
				const count = UI.getTalentCount(talent);
                let div = $('<div class="icon" data-count="' + count + '" data-x="' + talent.x + '" data-y="' + talent.y + '"></div>');
                div.html('<img src="dist/img/' + talent.iconname.toLowerCase() + '.jpg" alt="' + talent.n + '" />');
                if (count >= talent.m) div.addClass('maxed');
                if (talent.enable && count == 0) view.rotation.find('[data-id="' + talent.enable + '"]').addClass('hidden');
                if (talent.enable && count > 0) view.rotation.find('[data-id="' + talent.enable + '"]').removeClass('hidden');
                div.append('<a href="https://classic.wowhead.com/spell=' + talent.s[count == 0 ? 0 : count - 1] + '" class="wh-tooltip"></a>');
                table.find('tr').eq(talent.y).children().eq(talent.x).append(div);
            }
            view.talents.append(table);
        }
    },

    getTalent: function (div) {
        let tree = div.parents('table').index();
        let x = div.data('x');
        let y = div.data('y');
        for (let talent of talents[tree - 1].t)
            if (talent.x == x && talent.y == y)
                return talent;
    }



};
