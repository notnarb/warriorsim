// import {gear} from "./data/gear.js";
import {UI} from "./ui.js"
import {SETTINGS} from './settings.js'
import {STATS} from './stats';

(window as any).step = 0;

$(document).ready(function () {
	UI.init();
	SETTINGS.init();
	STATS.init();
});
