import {Simulation} from "./classes/simulation";
import {Player} from "./classes/player";

(self as any).step = 0;

onmessage = (event: MessageEvent) => {
	console.log('got event', event);
	const playerConfig = event.data.playerConfig;
	const simConfig = event.data.simConfig;

	const player = new Player(playerConfig);
	console.log('player', player);
	const sim = new Simulation(simConfig, player, (finished: any) => {
		(self.postMessage as any)({
			action: "finished",
			totalDmg: sim.totaldmg,
			totalDuration: sim.totalduration,
			startTime: sim.starttime,
			endTime: sim.endtime,
			minDps: sim.mindps,
			maxDps: sim.maxdps
		});
		console.log('finished', finished, sim)
	}, (iteration: number) => {
		// console.log('update', update, sim);
		(self.postMessage as any)({
			action: "update",
			totalDmg: sim.totaldmg,
			totalDuration: sim.totalduration,
			iteration: iteration,
			iterations: sim.iterations
		});
	})

	sim.start();
};
