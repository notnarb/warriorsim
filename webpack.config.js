var webpack = require('webpack');
var path = require('path');

module.exports = {
	mode: 'development',
	devtool: false, // using SourceMapDevToolPlugin.
	entry: {
		main: './js/main.ts',
		sim_worker: './js/sim_worker.ts',
	},
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: 'ts-loader',
				exclude: /node_modules/,
			},
		],
	},
	resolve: {
		extensions: [ '.ts', '.js' ],
	},
	plugins: [
		new webpack.SourceMapDevToolPlugin({
			filename: "dist/[name].js.map",
		})
	],
	devServer: {
		contentBase: __dirname,
		compress: true,
		port: 9000,
		publicPath: '/dist/',
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'dist'),
		publicPath: '/dist/',
	},
	watchOptions: {
		ignored: /\.#|node_modules/,
    },
};
