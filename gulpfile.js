var gulp = require('gulp');
var cssnano = require('gulp-cssnano');
var sass = require('gulp-sass');

gulp.task('sass', function(){
   return gulp.src('scss/style.scss')
      .pipe(sass())
      .pipe(cssnano())
      .pipe(gulp.dest('dist/css'));
});

gulp.task('watch', function(){
    gulp.watch('scss/*.scss', ['sass']);
});

gulp.task('default', ['sass', 'watch']);
